# LHCb Nightly Builds Configuration

This project hosts the Python modules used to configure LHCb Nightly
Builds.

## Introduction

The project is a standard Python project managed with the tool
[uv](https://docs.astral.sh/uv/), and provides the one package
`lhcbnightlyconf` which main object is the variable `slots`, a list of
`Slot` instances from `lb.nightly.configuration` (see
[lb-nightly-configuration](https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-configuration)
project).

The package contains several modules that are used to group the code
that creates the various slot instances and common functionalities and
constants. The `Slot` instances produced in the modules are collected by
the top level `__init__.py` to be added to `lhcbnightlyconf.slots`.

See the [CONTRIBUTING](CONTRIBUTING.rst) document for details on how to
add or modify slots and how to test the configuration.
