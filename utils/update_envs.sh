#!/bin/bash -e
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Helper script to update the conda environment files for the nightly builds
#

conda_cmd=
for cmd in mamba conda ; do
    if which ${cmd} >&/dev/null ; then
        conda_cmd=${cmd}
        break
    fi
done

if [ -z "${conda_cmd}" ] ; then
    echo "error: cannot find a conda equivalent command"
    exit 1
fi

for template in $(dirname $0)/../lhcbnightlyconf/env/*-template.yml ; do
    for conda_arch in linux-64 linux-aarch64 ; do
        env_name=$(basename ${template/-template.yml/})-${conda_arch}
        if [ $env_name != scheduler_env-linux-aarch64 ] ; then
            echo "Processing ${env_name}..."
            CONDA_SUBDIR=${conda_arch} ${conda_cmd} env create --name tmp-${env_name} --file ${template}
            ${conda_cmd} env export --name tmp-${env_name} | grep -E -v '^(name|prefix):' > $(dirname ${template})/${env_name}.yml
            sed -i 's/  - conda-forge/  - conda-forge\n  - nodefaults/' $(dirname ${template})/${env_name}.yml
            echo -e "variables:\n  CONDA_SUBDIR: ${conda_arch}" >> $(dirname ${template})/${env_name}.yml
            ${conda_cmd} env remove --yes --quiet --name tmp-${env_name}
        fi
    done
done
