# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from lb.nightly.configuration import (DBASE, PARAM, Package, Project, Slot,
                                      slot_factory)

from .Common import (DEFAULT_CACHE_ENTRIES, DEFAULT_GAUDI_RELEASED,
                     DEFAULT_GAUDI_VERSION, ERROR_EXCEPTIONS, PLATFORMS_RUN2,
                     WARNINGS_EXCEPTIONS)
from .Main import LCG_DEFAULT_VERSION

DEFAULT_NAMES_RUN2 = [
    "LHCb",
    "Lbcom",
    "Rec",
    "Brunel",
    "Phys",
    "Stripping",
    "Analysis",
    "DaVinci",
]

PROJECT_NAMES = {
    "2016":
    DEFAULT_NAMES_RUN2,
    "2017":
    DEFAULT_NAMES_RUN2 + ["Gaudi", "Hlt", "Moore", "LHCbIntegrationTests"],
    "2018":
    DEFAULT_NAMES_RUN2 + ["Gaudi", "Hlt", "Moore", "LHCbIntegrationTests"],
    "run2":
    DEFAULT_NAMES_RUN2 + [
        "LCG",
        "Gaudi",
        "Boole",
        "Alignment",
        "Castelao",
        "Urania",
        "Bender",
    ],
    "2023":
    ["LCG", "Gaudi", "Detector", "LHCb", "Lbcom", "Rec", "Allen", "Moore"],
    "2024": [
        "LCG",
        "Gaudi",
        "Detector",
        "LHCb",
        "Lbcom",
        "Rec",
        "Allen",
        "Moore",
        "DaVinci",
        "LHCbIntegrationTests",
        "Online",
        "Alignment",
        "MooreOnline",
        "Panoptes",
    ]
}

SPECIAL_VERSIONS = {
    "2016": {},
    "2017": {
        "Gaudi": "v28r2-patches"
    },
    "2018": {
        "Bender": "run2-patches",
        "Gaudi": "v29-patches",
    },
    "run2": {
        "LCG": LCG_DEFAULT_VERSION,
        "Gaudi": DEFAULT_GAUDI_VERSION,
        "LHCbIntegrationTests": "HEAD",
        "Urania": "HEAD",
    },
    "2023": {
        "LCG": "103",
        "Gaudi": "v36-patches",
    },
    "2024": {
        "LCG": "105a",
        "Gaudi": "v38r1p1",
        "Online": "master",
        "Boole": "master",
    }
}

PLATFORMS = {
    "2016": ["x86_64-slc6-gcc49-opt", "x86_64-slc6-gcc49-dbg"],
    "2017": ["x86_64-centos7-gcc62-opt", "x86_64-centos7-gcc62-dbg"],
    "2018": ["x86_64-centos7-gcc62-opt", "x86_64-centos7-gcc62-dbg"],
    "run2":
    PLATFORMS_RUN2,
    "2023": [
        "x86_64_v2-centos7-gcc12-opt",
        "x86_64_v2-centos7-gcc12-dbg",
        "x86_64_v2-el9-gcc12-dbg",
        "x86_64_v2-centos7-gcc12+detdesc-opt",
        "x86_64_v2-centos7-gcc12+detdesc-dbg",
    ],
    "2024": [
        "x86_64_v2-el9-gcc13-opt",
        "x86_64_v2-el9-gcc13-dbg",
        "x86_64_v3-el9-gcc13-opt+g",
        "x86_64_v2-el9-clang16-opt",
        "x86_64_v2-el9-clang16+detdesc-opt",
        "x86_64_v3-el9-gcc12+cuda12_1-opt+g",
        "x86_64_v2-el9-gcc13+detdesc-opt",
        "x86_64_v2-el9-gcc13+detdesc-dbg",
        "x86_64_v3-el9-gcc13+detdesc-opt+g",
    ],
}

OVERRIDE_CMAKE_VERSION = {
    "2016": "/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.17.3/Linux-x86_64/bin",
    "2017": "/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.17.3/Linux-x86_64/bin",
    "2018": "/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.17.3/Linux-x86_64/bin",
}

OVERRIDE_CCACHE_VERSION = {
    "2017": "/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/stable/linux-64/bin/ccache",
    "2018": "/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/stable/linux-64/bin/ccache",
}


@slot_factory
def preparePatchesSlot(year, name=None, LCG=None, apply_merge_requests=True):
    LCG = LCG or SPECIAL_VERSIONS[year].get("LCG", LCG_DEFAULT_VERSION)

    project_names = PROJECT_NAMES[year]

    slot = Slot(
        name if name else "lhcb-{0}-patches".format(year),
        desc="Test slot with patches for {0} production stack".format(year),
        projects=[
            Project(
                name,
                SPECIAL_VERSIONS[year].get(name, year + "-patches"),
                checkout=("git", {
                    "merges": "all"
                }) if apply_merge_requests else None,
            ) for name in project_names
        ],
        platforms=PLATFORMS[year],
        warning_exceptions=list(WARNINGS_EXCEPTIONS),
        error_exceptions=list(ERROR_EXCEPTIONS),
        cache_entries=dict(DEFAULT_CACHE_ENTRIES),
        env=[],
    )
    if year in OVERRIDE_CMAKE_VERSION:
        slot.env.append("PATH={}:$PATH".format(OVERRIDE_CMAKE_VERSION[year]))
    if year in OVERRIDE_CCACHE_VERSION:
        slot.cache_entries["ccache_cmd"] = OVERRIDE_CCACHE_VERSION[year]

    if hasattr(slot, "LCG"):
        slot.LCG.version = LCG
        slot.LCG.disabled = True

    for project in slot.projects:
        if project.version == "master":
            # ignore MRs for master
            project.checkout_opts = {}
    # this is needed to make LHCbIntegrationTests find the right vesions
    if "LHCbIntegrationTests" in project_names:
        for project in slot.projects:
            slot.cache_entries[project.name + "_version"] = project.version
    # setup data packages
    slot.projects.append(
        DBASE(packages=[
            Package("PRConfig", "HEAD"),
            Package("AppConfig", "HEAD")
        ]))
    slot.projects.append(PARAM(packages=[Package("ParamFiles", "HEAD")]))
    if year in ("2018", "run2"):
        slot.DBASE.packages.extend([
            Package("TurboStreamProd", "HEAD"),
            Package("TCK/L0TCK", "HEAD"),
        ])
    if year in {"2017"}:
        # Workaround for https://gitlab.cern.ch/gaudi/Gaudi/-/issues/123
        slot.cache_entries["HOST_BINARY_TAG"] = "x86_64-centos7-gcc62-opt"

    if hasattr(slot, "Allen"):
        node_overrides = {
            f"Allen/{platform}": "cuda-tests"
            for platform in slot.platforms if "cuda" in platform
        }
        if node_overrides:
            slot.metadata["node_overrides"] = node_overrides

    return slot


slots = [preparePatchesSlot(t) for t in ["2016", "2017", "2018"]]

run2 = preparePatchesSlot("run2")
run2.Gaudi.disabled = DEFAULT_GAUDI_RELEASED
slots.append(run2)

run2_prerelease = preparePatchesSlot(
    "run2",
    name="lhcb-run2-prerelease",
    apply_merge_requests=False,
    LCG=LCG_DEFAULT_VERSION,
)
run2_prerelease.Gaudi.version = DEFAULT_GAUDI_VERSION
run2_prerelease.Gaudi.disabled = DEFAULT_GAUDI_RELEASED
run2_prerelease.disabled = True
slots.append(run2_prerelease)

run2_gaudi_head = preparePatchesSlot(
    "run2",
    name="lhcb-run2-gaudi-head",
    LCG="105c",
)
run2_gaudi_head.Gaudi.version = "HEAD"
run2_gaudi_head.Gaudi.checkout_opts["merges"] = ["label=lhcb-gaudi-head"]
run2_gaudi_head.enabled = True
run2_gaudi_head.platforms = PLATFORMS_RUN2
slots.append(run2_gaudi_head)

slot2023 = preparePatchesSlot("2023")
slot2023.desc = (
    slot2023.desc + "; used for: " +
    "PbPb (HLT2 + Sprucing), VdM/BGI (HLT2), 2022 reprocessing (Sprucing)")
slots.append(slot2023)

slot2024 = preparePatchesSlot("2024")
slot2024.Gaudi.disabled = slot2024.Gaudi.version == DEFAULT_GAUDI_VERSION and DEFAULT_GAUDI_RELEASED
slots.append(slot2024)
