###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import pytest

from lhcbnightlyconf.env import get_environment

myenv = {
    "channels": ["conda-forge", "nodefaults"],
    "dependencies": [
        "python=3.7",
        "cmake=3.18.4",
        "ninja=1.10.1",
        "ccache=3.7.12",
        "pip=20.2.4",
        {
            "pip": [
                "certifi==2020.11.8",
                "chardet==3.0.4",
                "cloudant==2.14.0",
                "gitdb==4.0.5",
                "gitpython==3.1.11",
                "idna==2.10",
                "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-configuration.git@master",
                "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-db.git@master",
                "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-functions.git@master",
                "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-utils.git@master",
                "python-gitlab==2.5.0",
                "pyyaml==5.3.1",
                "requests==2.24.0",
                "semver==2.13.0",
                "smmap==3.0.4",
                "spython==0.0.85",
                "tabulate==0.8.7",
                "urllib3==1.25.11",
            ]
        },
    ],
}


def test_update_conda():
    assert get_environment([{"package": "cmake", "version": "3.19"}], env=myenv) == {
        "channels": ["conda-forge", "nodefaults"],
        "dependencies": [
            "python=3.7",
            "cmake=3.19",
            "ninja=1.10.1",
            "ccache=3.7.12",
            "pip=20.2.4",
            {
                "pip": [
                    "certifi==2020.11.8",
                    "chardet==3.0.4",
                    "cloudant==2.14.0",
                    "gitdb==4.0.5",
                    "gitpython==3.1.11",
                    "idna==2.10",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-configuration.git@master",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-db.git@master",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-functions.git@master",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-utils.git@master",
                    "python-gitlab==2.5.0",
                    "pyyaml==5.3.1",
                    "requests==2.24.0",
                    "semver==2.13.0",
                    "smmap==3.0.4",
                    "spython==0.0.85",
                    "tabulate==0.8.7",
                    "urllib3==1.25.11",
                ]
            },
        ],
    } # yapf: disable


def test_update_pip():
    assert get_environment([{"package": "idna", "version": "3"}], env=myenv) == {
        "channels": ["conda-forge", "nodefaults"],
        "dependencies": [
            "python=3.7",
            "cmake=3.18.4",
            "ninja=1.10.1",
            "ccache=3.7.12",
            "pip=20.2.4",
            {
                "pip": [
                    "certifi==2020.11.8",
                    "chardet==3.0.4",
                    "cloudant==2.14.0",
                    "gitdb==4.0.5",
                    "gitpython==3.1.11",
                    "idna==3",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-configuration.git@master",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-db.git@master",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-functions.git@master",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-utils.git@master",
                    "python-gitlab==2.5.0",
                    "pyyaml==5.3.1",
                    "requests==2.24.0",
                    "semver==2.13.0",
                    "smmap==3.0.4",
                    "spython==0.0.85",
                    "tabulate==0.8.7",
                    "urllib3==1.25.11",
                ]
            },
        ],
    } # yapf: disable


def test_update_git():
    assert get_environment(
        [
            {
                "package": "lb-nightly-functions",
                "version": "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-functions.git@feature",
            }
        ], env=myenv
    ) == {
        "channels": ["conda-forge", "nodefaults"],
        "dependencies": [
            "python=3.7",
            "cmake=3.18.4",
            "ninja=1.10.1",
            "ccache=3.7.12",
            "pip=20.2.4",
            {
                "pip": [
                    "certifi==2020.11.8",
                    "chardet==3.0.4",
                    "cloudant==2.14.0",
                    "gitdb==4.0.5",
                    "gitpython==3.1.11",
                    "idna==2.10",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-configuration.git@master",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-db.git@master",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-functions.git@feature",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-utils.git@master",
                    "python-gitlab==2.5.0",
                    "pyyaml==5.3.1",
                    "requests==2.24.0",
                    "semver==2.13.0",
                    "smmap==3.0.4",
                    "spython==0.0.85",
                    "tabulate==0.8.7",
                    "urllib3==1.25.11",
                ]
            },
        ],
    } # yapf: disable


def test_update_new_conda():
    assert get_environment([{"package": "new-pkg", "version": "1"}], env=myenv) == {
        "channels": ["conda-forge", "nodefaults"],
        "dependencies": [
            "python=3.7",
            "cmake=3.18.4",
            "ninja=1.10.1",
            "ccache=3.7.12",
            "pip=20.2.4",
            "new-pkg=1",
            {
                "pip": [
                    "certifi==2020.11.8",
                    "chardet==3.0.4",
                    "cloudant==2.14.0",
                    "gitdb==4.0.5",
                    "gitpython==3.1.11",
                    "idna==2.10",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-configuration.git@master",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-db.git@master",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-functions.git@master",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-utils.git@master",
                    "python-gitlab==2.5.0",
                    "pyyaml==5.3.1",
                    "requests==2.24.0",
                    "semver==2.13.0",
                    "smmap==3.0.4",
                    "spython==0.0.85",
                    "tabulate==0.8.7",
                    "urllib3==1.25.11",
                ]
            },
        ],
    } # yapf: disable


def test_update_new_pip():
    assert get_environment([{"package": "newer-pkg", "version": "2", "pip": True}], env=myenv) == {
        "channels": ["conda-forge", "nodefaults"],
        "dependencies": [
            "python=3.7",
            "cmake=3.18.4",
            "ninja=1.10.1",
            "ccache=3.7.12",
            "pip=20.2.4",
            {
                "pip": [
                    "certifi==2020.11.8",
                    "chardet==3.0.4",
                    "cloudant==2.14.0",
                    "gitdb==4.0.5",
                    "gitpython==3.1.11",
                    "idna==2.10",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-configuration.git@master",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-db.git@master",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-functions.git@master",
                    "git+https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-utils.git@master",
                    "python-gitlab==2.5.0",
                    "pyyaml==5.3.1",
                    "requests==2.24.0",
                    "semver==2.13.0",
                    "smmap==3.0.4",
                    "spython==0.0.85",
                    "tabulate==0.8.7",
                    "urllib3==1.25.11",
                    "newer-pkg==2",
                ]
            },
        ],
    } # yapf: disable


def test_update_no_pip():
    myenv = {"dependencies": ["cmake=4"]}
    assert get_environment([{"package": "cmake", "version": "3.19"}], env=myenv) == {
        "dependencies": ["cmake=3.19"]
    } # yapf: disable
    assert get_environment(
        [{"package": "newer-pkg", "version": "2", "pip": True}], env=myenv
    ) == {"dependencies": ["cmake=4", {"pip": ["newer-pkg==2"]}]} # yapf: disable


def test_update_bad_env():
    myenv = {}
    with pytest.raises(ValueError):
        get_environment([{"package": "cmake", "version": "3.19"}], env=myenv)
    with pytest.raises(ValueError):
        get_environment([{"package": "cmake"}])
    with pytest.raises(ValueError):
        get_environment([{"version": "2"}])
    with pytest.raises(ValueError):
        get_environment([{}])
