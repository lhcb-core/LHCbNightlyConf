###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from .Common import (ALL_PROJECTS, DEFAULT_GAUDI_RELEASED,
                     DEFAULT_GAUDI_VERSION, MASTER_CITEST_PLATFORMS,
                     MASTER_PLATFORMS)
from .Main import create_head_slot

master = create_head_slot(
    'lhcb-master', projects=ALL_PROJECTS, platforms=MASTER_PLATFORMS)
master.desc = 'master of everything (without merge requests)'
# set all projects version to "master", except LCG and data packages containers
for project in master.projects:
    if project.name not in ("LCG", "DBASE", "PARAM"):
        project.version = "master"
# override version of specific projects
master.Gaudi.version = DEFAULT_GAUDI_VERSION
master.Gaudi.disabled = DEFAULT_GAUDI_RELEASED
master.metadata["ci_test_platforms"] = list(MASTER_CITEST_PLATFORMS)
master.cache_entries["GAUDI_TEST_PUBLIC_HEADERS_BUILD"] = True

slots = [master]
