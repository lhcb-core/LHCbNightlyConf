# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from lb.nightly.configuration import Project, Slot

projects_davinci_51 = [
    Project('DaVinci', 'DV4Strip26', checkout=('git', {
        'merges': [51]
    })),
]

slot_davinci_51 = Slot(
    'lhcb-test-davinci-51',
    disabled=True,
    desc=
    'test MR <a href="https://gitlab.cern.ch/lhcb/DaVinci/merge_requests/51" target="_blank">lhcb/DaVinci!51</a>',
    projects=projects_davinci_51,
    env=[],
    platforms=[
        'x86_64-slc6-gcc48-opt',
        'x86_64-slc6-gcc48-dbg',
        'x86_64-slc6-gcc49-opt',
        'x86_64-slc6-gcc49-dbg',
    ],
)

projects_davinci_v42r7p3 = [
    Project('DaVinci', '2017-patches'),
    Project('LHCbIntegrationTests', 'HEAD'),
]

slot_davinci_v42r7p3 = Slot(
    'lhcb-test-davinci-v42r7p3',
    disabled=True,
    desc='prerelease slot for DaVinci v42r7p3',
    projects=projects_davinci_v42r7p3,
    env=[],
    platforms=[
        'x86_64-slc6-gcc62-dbg',
        'x86_64-slc6-gcc62-opt',
        'x86_64-centos7-gcc62-opt',
        'x86_64-centos7-gcc62-dbg',
    ],
)

# this is needed to make LHCbIntegrationTests find the right vesions
slot_davinci_v42r7p3.cache_entries['Moore_version'] = 'v26r6p1'

slots = [slot_davinci_51, slot_davinci_v42r7p3]
