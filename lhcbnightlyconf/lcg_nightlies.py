# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import datetime
import os
import re

from lb.nightly.configuration import Project, slot_factory

from .Common import (ALL_PROJECTS, DEFAULT_CACHE_ENTRIES,
                     DEFAULT_REQUIRED_EXTERNALS, PLATFORMS_RUN2, LCGToolchains)
from .Main import create_head_slot
from .Patches import preparePatchesSlot

# list of defined slots
slots = []

TODAY = [
    'Sun',
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat',
    'Sun',
][datetime.date.today().isoweekday()]


def cmake_prefix_path(name):
    return 'CMAKE_PREFIX_PATH={0}'.format(':'.join([
        '/cvmfs/sft-nightlies.cern.ch/lcg/nightlies/{0}/{1}'.format(
            name, TODAY), '${CMAKE_PREFIX_PATH}'
    ]))


def set_desc(slot, root_version):
    slot.desc = (
        'head of everything against Gaudi <a href="https://gitlab.cern.ch/gaudi/Gaudi/tree/{gaudi_version}" target="_blank">{gaudi_version}</a> '
        'and today\'s LCG <a href="http://lcginfo.cern.ch/release/{name}/" target="_blank">{name}</a> slot '
        '(ROOT {root_version}, <a href="http://cdash.cern.ch/index.php?project=LCGSoft" target="_blank">status</a>)'
    ).format(
        gaudi_version=slot.Gaudi.version,
        name=slot.name.split('-')[-1],
        root_version=root_version)


PLATFORMS = [
    "x86_64_v2-el9-gcc13-opt",
    "x86_64_v2-el9-gcc13-dbg",
    "x86_64_v2-el9-clang16-opt",
    "x86_64_v2-el9-clang16-dbg",
    "x86_64_v2-el9-gcc13+detdesc-dbg",
    "armv8.1_a-el9-gcc13-opt",
    "armv8.1_a-el9-gcc13-dbg",
]

PROJECTS = [name for name in ALL_PROJECTS if name != "GaussinoExtLibs"
            ] + ["DD4hepDDG4"]


def create_lcg_slot(lcg_version):
    slot = create_head_slot(
        'lhcb-gaudi-head',
        LCG="{0}/{1}".format(lcg_version, TODAY),
        projects=PROJECTS,
        platforms=PLATFORMS)
    slot.name = 'lhcb-lcg-' + lcg_version
    slot.env = [
        cmake_prefix_path(lcg_version),
        "DISPLAY=",
        "FONTCONFIG_PATH=/etc/fonts",
        'LD_LIBRARY_PATH=${PWD}/build/GitCondDB/InstallArea/${platform}/lib:${LD_LIBRARY_PATH}',
    ]
    slot.cache_entries = dict(
        list(DEFAULT_CACHE_ENTRIES.items()) + [
            ('CMAKE_USE_CCACHE', False),
        ])
    slot.preconditions = [{
        "name": "lcgNightly",
        "args": {
            "path":
            '/cvmfs/sft-nightlies.cern.ch/lcg/nightlies/{0}/{1}/LCG_externals_${{BINARY_TAG}}.txt'
            .format(lcg_version, TODAY),
            "required":
            DEFAULT_REQUIRED_EXTERNALS
        }
    }]
    return slot


lcg = {
    'dev3': create_lcg_slot(lcg_version='dev3lhcb'),
    'dev4': create_lcg_slot(lcg_version='dev4lhcb'),
}

for dev in lcg.values():
    dev.projects.append(Project("GitCondDB", "HEAD"))
    dev.Detector._deps = ["GitCondDB"]

    dev.enabled = (TODAY == 'Sat')

# dev3lhcb contains HepMC3 instead of HepMC
if "HepMC" in lcg["dev3"].preconditions[0]["args"]["required"]:
    lcg["dev3"].preconditions[0]["args"]["required"].remove("HepMC")
    lcg["dev3"].preconditions[0]["args"]["required"].append("hepmc3")

# DD4hep from LCG builds requires Geant4
lcg['dev3'].Detector._deps.append("Geant4")
lcg['dev4'].Detector._deps.append("Geant4")

set_desc(lcg['dev3'], root_version='master')
set_desc(lcg['dev4'], root_version='6.32-patches')
slots.extend(lcg.values())

# Adding slot for run2-patches, based on the dev4 LCG nightly slot and Gaudi/HEAD
run2_dev4 = preparePatchesSlot(
    'run2', name='lhcb-run2-patches-dev4', LCG='dev4lhcb')
run2_dev4.env += lcg['dev4'].env
run2_dev4.cache_entries = lcg['dev4'].cache_entries
run2_dev4.preconditions = lcg['dev4'].preconditions
run2_dev4.Gaudi.version = 'HEAD'
run2_dev4.desc = "clone of run2-patches against next Gaudi and LCG version"
run2_dev4.platforms = PLATFORMS_RUN2
run2_dev4.enabled = lcg['dev4'].enabled
slots.append(run2_dev4)

# find latest rc build
rc_root = '/cvmfs/sft.cern.ch/lcg/releases'
if os.path.isdir(rc_root):
    rc_versions = [
        v for v in os.listdir(rc_root)
        if (re.match(r'LCG_\d*rc\d+$', v)
            or re.match(r'LCG_\d*rc\d+_LHCB_\d+$', v))
        and os.path.isdir(os.path.join(rc_root, v))
    ]
    # sort by number
    rc_versions.sort(
        key=lambda v: [int(i) for i in re.findall(r'\d+', v)], reverse=True)
    if rc_versions:  # create the slot only if there's one
        latest_rc = rc_versions[0]
        lcg_rc = create_head_slot(
            'lhcb-lcg-rc',
            LCG=latest_rc[4:],
            platforms=[
                l[14:-4] for l in os.listdir(os.path.join(rc_root, latest_rc))
                if re.match(r'LCG_externals_.*\.txt$', l)
            ])
        lcg_rc.desc = (
            'Special slot based on lhcb-gaudi-head to test LCG rc builds '
            '(current: %s)' % latest_rc)
        lcg_rc.enabled = False
        lcg_rc.env = lcg_rc.env + [
            'CMAKE_PREFIX_PATH=%s:${CMAKE_PREFIX_PATH}' % rc_root
        ]
        slots.append(lcg_rc)


@slot_factory
def test_lcg_version(version, platforms, name='lhcb-lcg-test'):
    lcg_test = create_head_slot(name, LCG=version, platforms=platforms)
    lcg_test.desc = (
        'Special slot based on lhcb-gaudi-head to test LCG %s' % version)
    lcg_test.enabled = False
    slots.append(lcg_test)
    return lcg_test


slot = test_lcg_version('101a', platforms=PLATFORMS)
slot.projects.append(LCGToolchains("add-lcg-101a"))
