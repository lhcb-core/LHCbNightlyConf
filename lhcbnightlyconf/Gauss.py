# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# datetime for rebuildAlways temporary hack
import datetime
import os

from lb.nightly.configuration import (DBASE, PARAM, Package, Project, Slot,
                                      slot_factory)

from .Common import ERROR_EXCEPTIONS, WARNINGS_EXCEPTIONS, LCGToolchains

DEFAULT_GEANT4_VERSION = 'v10r6p2t7'

# list of defined slots
slots = []

## A few reminder of possible ways to include merge requests
# gSim09Cmake = cloneSlot('lhcb-sim09', 'lhcb-sim09-cmake')
# Project('Gauss', 'Sim09', checkout=('git',{'merges':'all'})) ] )
# Project('Gauss', 'Sim09', checkout=('git',{'merges':10})) ] )
# Project('Gauss', 'Sim09', checkout=('git',{'merges':[10, 11, 12]})) ] )

# set cmt environment to pick up the nightlies for dbase and param
cmtEnv = [('CMTPROJECTPATH=' + '${CMTPROJECTPATH}' +
           ':/cvmfs/lhcbdev.cern.ch/lib/lcg/releases')]

# do the same for cmake
cmakeEnv = [('CMAKE_PREFIX_PATH=' + ':'.join([
    '/cvmfs/lhcbdev.cern.ch/lib/lcg/releases',
    '/cvmfs/lhcb.cern.ch/lib/lcg/releases',
    '/cvmfs/sft.cern.ch/lcg/releases',
    '${CMAKE_PREFIX_PATH}',
]))]

# to test release build
relEnv = [('CMAKE_PREFIX_PATH=' + ':'.join([
    '/cvmfs/lhcb.cern.ch/lib/lcg/releases',
    '${CMAKE_PREFIX_PATH}',
]))]

SIM09_PLATFORMS = [
    'x86_64-slc6-gcc48-opt', 'x86_64-slc6-gcc48-dbg', 'x86_64-slc6-gcc49-opt',
    'x86_64-slc6-gcc49-dbg'
]

#
# Slots for g4 and gauss with cmake
#    for Sim09
gSim09Cmake = Slot(
    name='lhcb-sim09-cmake',
    projects=[
        Project('LCGCMT', '79', disabled=True),
        Project('Geant4', 'Sim09', checkout=('git', {
            'merges': 'all'
        })),
        Project('LHCb', 'sim09-patches', checkout=('git', {
            'merges': 'all'
        })),
        Project('Gauss', 'Sim09', checkout=('git', {
            'merges': 'all'
        })),
        DBASE(packages=[
            Package('PRConfig', 'HEAD'),
            Package('Gen/PGunsData', 'HEAD')
        ]),
        PARAM(packages=[Package('ParamFiles', 'HEAD')])
    ])
gSim09Cmake.desc = 'Identical to lhcb-sim09 but built with cmake'
gSim09Cmake.platforms = SIM09_PLATFORMS
gSim09Cmake.warning_exceptions = list(WARNINGS_EXCEPTIONS)
gSim09Cmake.error_exceptions = list(ERROR_EXCEPTIONS)
gSim09Cmake.build_tool = 'cmake'
gSim09Cmake.env = list(cmakeEnv)
# use an older version of CMake on SLC6
gSim09Cmake.env.append(
    "PATH=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.17.3/Linux-x86_64/bin:$PATH")
gSim09Cmake.cache_entries['GEANT4_TAG'] = 'lhcb/v9.6.4-branch'
gSim09Cmake.cache_entries['GAUDI_DIAGNOSTICS_COLOR'] = False
gSim09Cmake.cache_entries['GAUDI_DIAGNOTICS_COLOR'] = False
# Temp hack to trigger slot rebuild
gSim09Cmake.metadata["rebuildAlways"] = str(datetime.date.today())
slots.append(gSim09Cmake)

# PLATFORMS FOR SIM10 and SIM11
SIM10_PLATFORMS = [
    'x86_64_v2-centos7-gcc11-opt', 'x86_64_v2-centos7-gcc11-dbg'
]

DEV10_PLATFORMS = SIM10_PLATFORMS

DEV10GH_PLATFORMS = [
    'x86_64_v2-el9-gcc13-opt', 'x86_64_v2-el9-gcc13-dbg',
    'armv8.1_a-el9-gcc13-opt'
]

SIM11_PLATFORMS = [
    'x86_64_v2-el9-gcc13-opt',
    'x86_64_v2-el9-gcc13-dbg',
]

SIM11_WITH_DETDESC_PLATFORMS = SIM11_PLATFORMS + [
    'x86_64_v2-el9-gcc13+detdesc-opt',
    'x86_64_v2-el9-gcc13+detdesc-dbg',
]

REL11_PLATFORMS = SIM11_WITH_DETDESC_PLATFORMS + ['armv8.1_a-el9-gcc13-opt']

DEV11_PLATFORMS = SIM11_PLATFORMS + [
    'x86_64_v3-el9-gcc13-opt+g', 'armv8.1_a-el9-gcc13-opt'
]


# Gauss Sim10 slots: on LHCb sim10-patches, Geant4 10.6.2.X single threaded, on Sim10/sim10-patches branch
@slot_factory
def create_sim_slot(name, desc, platforms=SIM10_PLATFORMS, with_merges=True):
    checkout = lambda: ('git', {'merges': 'all'} if with_merges else {})
    slot = Slot(
        name=name,
        projects=[
            Project('LCG', '102b', disabled=True),
            Project('Geant4', 'v10r6p2t7', disabled=True),
            Project('Gaudi', 'v36r9p1', disabled=True),
            Project('LHCb', 'sim10-patches', checkout=checkout()),
            Project('Run2Support', 'sim10-patches', checkout=checkout()),
            Project('Gauss', 'Sim10', checkout=checkout()),
            DBASE(packages=[Package('PRConfig', 'HEAD')])
        ],
        warning_exceptions=list(WARNINGS_EXCEPTIONS),
        error_exceptions=list(ERROR_EXCEPTIONS))
    slot.desc = desc
    slot.platforms = list(platforms)
    slot.build_tool = 'cmake'
    slot.env = cmakeEnv
    slot.cache_entries['GAUDI_LEGACY_CMAKE_SUPPORT'] = True
    return slot


@slot_factory
def create_gaussino_slot(name, desc, platforms=SIM11_PLATFORMS):
    slot = Slot(
        name=name,
        desc=desc,
        projects=[
            Project('LCG', '105c', disabled=True),
            Project('Gaudi', 'v39r2'),
            Project('Geant4', 'HEAD'),
            Project('Detector', 'HEAD'),
            # LHCb/master is not compatible with LCG 105c
            Project(
                'LHCb',
                'lcg-105-compat-patches',
                checkout=('git', {
                    'merges': 'all'
                }),
            ),
            Project('Run2Support', 'HEAD'),
            Project(
                'GaussinoExtLibs',
                'HEAD',
                checkout=('git', {
                    'url':
                    'https://gitlab.cern.ch/Gaussino/gaussinoextlibs.git'
                })),
            Project(
                'Gaussino',
                'HEAD',
                checkout=('git', {
                    'url': 'https://gitlab.cern.ch/Gaussino/Gaussino.git'
                })),
            Project('Gauss', 'HEAD'),
            DBASE(packages=[
                Package('PRConfig', 'HEAD'),
                Package(
                    'Gen/DecFiles',
                    'Sim11v1',
                    checkout=('git', {
                        'merges': 'all'
                    })),
                Package('AppConfig', 'HEAD')
            ])
        ],
        warning_exceptions=list(WARNINGS_EXCEPTIONS),
        error_exceptions=list(ERROR_EXCEPTIONS))

    slot.env = list(cmakeEnv)  # ensure we get a local copy of an object
    slot.cache_entries['GEANT4_BUILD_MULTITHREADED'] = True
    slot.cache_entries['GAUDI_LEGACY_CMAKE_SUPPORT'] = True
    slot.platforms = list(platforms)
    slot.build_tool = 'cmake'
    return slot


# Slot for Gauss Sim10 development - for final MR check
gaussSim10 = create_sim_slot(
    'lhcb-sim10',
    'Build of Gauss for Sim10 against LHCb sim10-patches, Gaudi v36r5 and LCG_101_LHCB_7, Geant4 v10r6p2t6',
    with_merges=False,
)
gaussSim10.metadata["rebuildAlways"] = str(datetime.date.today())
gaussSim10.DBASE.packages.extend([
    Package('Gen/MadgraphData', 'HEAD'),
    Package('Gen/DecFiles', 'Sim10', checkout=('git', {
        'merges': 'all'
    })),
    Package('AppConfig', 'v3-patches', checkout=('git', {
        'merges': 'all'
    }))
])
slots.append(gaussSim10)

# Slot for Gauss Sim10 development - build against Gaudi HEAD
gaussSim10Gaudi = create_sim_slot(
    'lhcb-sim10-gaudi-head',
    'clone of lhcb-sim10 against next Gaudi and LCG',
    platforms=DEV10GH_PLATFORMS)
gaussSim10Gaudi.LCG.version = '105c'
gaussSim10Gaudi.Geant4.disabled = False
gaussSim10Gaudi.Geant4.version = 'sim10-patches'
gaussSim10Gaudi.Geant4.checkout_opts["merges"] = ["all"]
gaussSim10Gaudi.Gaudi.version = 'HEAD'
gaussSim10Gaudi.Gaudi.disabled = False
#gaussSim10Gaudi.Gaudi.checkout_opts["merges"] = ["label=lhcb-gaudi-head"]
slots.append(gaussSim10Gaudi)

# Slot for Gauss Sim10 against Geant 10.7.x as to allow comparison against Sim11
gaussSim10G4107 = create_sim_slot(
    'lhcb-sim10-g4-v10r7',
    'clone of lhcb-sim10 built with Geant4 10.7.x as in Sim11 (aka Gauss-on-Gsino)'
)
gaussSim10G4107.Geant4.version = 'HEAD'
gaussSim10G4107.Geant4.disabled = False
gaussSim10G4107.cache_entries['GEANT4_BUILD_MULTITHREADED'] = False
slots.append(gaussSim10G4107)

# Slots for Gauss Sim10 generator development
gaussSim10Gen = create_sim_slot(
    'lhcb-sim10-gen',
    'clone of lhcb-sim10 but with new generators or generator version')
gaussSim10Gen.DBASE.packages.extend([
    Package('Gen/PowhegboxData', 'HEAD'),
    Package('Gen/DecFiles', 'Sim10', checkout=('git', {
        'merges': 'all'
    }))
])
gaussSim10Gen.projects.append(LCGToolchains("gcorti-master-patch-95676"))
slots.append(gaussSim10Gen)

# Slot for Gauss with Lamarr development
gaussSim10Fast = create_sim_slot(
    'lhcb-sim10-fastsim',
    'Development build of Gauss identical to lhcb-sim10 but to test fast simulations, e.g. Lamarr'
)
gaussSim10Fast.DBASE.packages.append(Package('LamarrData', 'HEAD'))
slots.append(gaussSim10Fast)

# Slot for Sim10 developmment, integration of all pending MR to be tested
gaussSim10Dev = create_sim_slot(
    'lhcb-sim10-dev',
    'close of lhcb-sim10 with more recent patches, also in LHCb sim10-patches, Run2Support and Geant4 10.6.2.7',
    platforms=DEV10_PLATFORMS)
gaussSim10Dev.DBASE.packages.extend([
    Package('Gen/DecFiles', 'Sim10', checkout=('git', {
        'merges': 'all'
    })),
    Package('LamarrData', 'HEAD'),
])
gaussSim10Dev.Geant4.disabled = False
gaussSim10Dev.Geant4.version = 'sim10-patches'
gaussSim10Dev.Geant4.checkout_opts["merges"] = ["all"]
gaussSim10Dev.metadata["rebuildAlways"] = str(datetime.date.today())
slots.append(gaussSim10Dev)

# Slot for Sim11 to check candidate release
gaussSim11 = create_gaussino_slot(
    'lhcb-sim11',
    'Build of Gauss-on-Gsino for Sim11 for release',
    platforms=REL11_PLATFORMS)
gaussSim11.projects.extend(
    [Project('Lbcom', 'HEAD'),
     Project('Boole', 'HEAD')])
gaussSim11.metadata["rebuildAlways"] = str(datetime.date.today())
slots.append(gaussSim11)

# Slot for Sim11 to check future MR not yet ready for release but that need stable environment
gaussSim11Dev = create_gaussino_slot(
    'lhcb-sim11-dev',
    'Build of Gauss-on-Gsino to test simulation MR not yet for release independently of LHCb development',
    platforms=DEV11_PLATFORMS)
slots.append(gaussSim11Dev)

# Slot for Sim11 run5 to check future MR not yet ready for release but that need stable environment
gaussSim11Run5 = create_gaussino_slot(
    'lhcb-sim11-run5',
    'Build of Gauss-on-Gsino to test simulation MR for run5 not yet for release independently of LHCb development',
    platforms=DEV11_PLATFORMS)
slots.append(gaussSim11Run5)

# Slot for Sim11 for Rich+Torch combination
gaussSim11RichTorch = create_gaussino_slot(
    'lhcb-sim11-rich-torch',
    'Build of Gauss-on-Gsino for Rich+Torch combinations',
    platforms=SIM11_PLATFORMS)
gaussSim11RichTorch.Geant4.version = 'richTorch'
gaussSim11RichTorch.Detector.version = 'richTorch'
gaussSim11RichTorch.LHCb.version = 'richTorch'
gaussSim11RichTorch.Run2Support.version = 'richTorch'
gaussSim11RichTorch.Gauss.version = 'richTorch'
slots.append(gaussSim11RichTorch)

# Slot for Gaussino and Gauss-on-Gaussino with Fast Simulations (Interface requires Geant4 10.7)
gaussSim11Fast = create_gaussino_slot(
    'lhcb-sim11-fastsim',
    'Test build of Fast Simulation Interface in Gaussino and Gauss-on-Gaussino',
    platforms=SIM11_WITH_DETDESC_PLATFORMS,
)
gaussSim11Fast.DBASE.packages.append(Package('FastCaloSimData', 'HEAD'))
slots.append(gaussSim11Fast)

# Slot for Gaussino and Gauss-on-Gaussino generators development
gaussSim11Gen = create_gaussino_slot(
    'lhcb-sim11-gen',
    'Build of Gauss-on-Gsino to test new generators and new generators versions'
)
gaussSim11Gen.projects.append(LCGToolchains("gcorti_lcg105c_pythia8_310"))
slots.append(gaussSim11Gen)

# Slot for Gaussino and Gauss-on-Gaussino against future versions of Geant 10.7.x to start  with and later G4 11.
gaussSim11G4 = create_gaussino_slot(
    'lhcb-sim11-g4',
    'clone of lhcb-sim11 built with newer version of Geant4',
)
gaussSim11G4.DBASE.packages.extend([Package('Geant4Files', 'HEAD')])
gaussSim11G4.Geant4.version = 'HEAD'
slots.append(gaussSim11G4)

# Slot for Gaussino only
#   to become standlone gaussino build
gaussino = create_gaussino_slot(
    'lhcb-gaussino',
    'Test build of Gaussino and future Gauss',
)
gaussino.DBASE.packages.extend([Package('Geant4Files', 'HEAD')])
gaussino.Run2Support.disabled = True
gaussino.Gauss.disabled = True
slots.append(gaussino)
