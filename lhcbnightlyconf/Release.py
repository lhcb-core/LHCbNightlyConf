# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Logic for special lhcb-release slot.

The content of the release slot is driven by special environment variables
usually provided by dedicated `Jenkins jobs <>`_.

projects_list
  whitespace separated list of project names and versions, e.g.::

      Gaudi v32r1 LHCb v42r2 Lbcom v23r0p1

packages_list
  whitespace separated list of project names and versions, where the name of
  the package must be prefixed with ``PARAM:`` for packages in the *PARAM*
  project, e.g.::

      AppConfig v3r384 WG/CharmWGProd v1r60 PARAM:TMVAWeights v1r15

platforms
  whitespace or comma separated list of platforms to build (leave empty for data packages)

build_tool
  build tool to use (``cmt`` or ``cmake``)
'''

import os

from lb.nightly.configuration import DBASE, PARAM, Package, Project, Slot

try:
    from LbEnv import fixProjectCase
except ImportError:
    # FIXME: when using lb.nightly.configuration LbEnv is not available
    fixProjectCase = lambda x: x

# All slots defined in this module
slots = []

# Legacy error and warning exceptions for release slots
ERR_EXCEPT = ["distcc\\[", "::error::", "^ *Error *$"]
WARN_EXCEPT = [
    ".*/boost/.*", "^--->> genreflex: WARNING:.*", " note:", "distcc\\[",
    ("Warning\\:\\ The\\ tag\\ (use-distcc|no-pyzip|"
     "LCG\\_NIGHTLIES\\_BUILD|COVERITY|"
     "use\\-dbcompression)\\ is\\ not\\ used.*"),
    ".*#CMT---.*Warning: Structuring style used.*", ".*/Boost/.*warning:.*",
    ".*/ROOT/.*warning:.*",
    (".*stl_algo.h:[0-9]+: warning: array subscript is above array "
     "bounds")
]


def get_pairs(data):
    '''
    Pick elements from a list by pairs:

    >>> get_pairs([0, 1, 2, 3])
    [(0, 1), (2, 3)]
    '''
    return list(zip(data[::2], data[1::2]))


# Check minimal precondition before creating the slot
if 'projects_list' in os.environ or 'packages_list' in os.environ:
    data_projects = []
    dbase_pkgs = [
        Package(
            name.replace('DBASE:', ''),
            version,
            checkout_opts={'export': True}) for name, version in get_pairs(
                os.environ.get('packages_list', '').strip().split())
        if not name.startswith('PARAM:')
    ]
    if dbase_pkgs:
        data_projects.append(DBASE(packages=dbase_pkgs))
    param_pkgs = [
        Package(
            name.replace('PARAM:', ''),
            version,
            checkout_opts={'export': True}) for name, version in get_pairs(
                os.environ.get('packages_list', '').strip().split())
        if name.startswith('PARAM:')
    ]
    if param_pkgs:
        data_projects.append(PARAM(packages=param_pkgs))

    slot = Slot(
        'lhcb-release',
        projects=data_projects + [
            Project(
                fixProjectCase(name), version, checkout_opts={'export': True})
            for name, version in get_pairs(
                os.environ.get('projects_list', '').strip().split())
        ],
        platforms=os.environ.get('platforms', '').replace(',',
                                                          ' ').strip().split(),
        build_tool=os.environ.get('build_tool', 'cmake').strip().lower(),
        no_patch=True,
        with_version_dir=True,
        cache_entries={'GAUDI_STRICT_VERSION_CHECK': True})

    slot.desc = 'Release slot for ' + ' '.join(
        ' '.join(map(str, p.packages)) if hasattr(p, 'packages') else str(p)
        for p in slot.projects)

    if hasattr(slot, 'Geant4'):
        slot.Geant4.with_shared = True

    slots.append(slot)
