###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from lb.nightly.configuration import DBASE, Package, Project, Slot

from .Common import (DEFAULT_CACHE_ENTRIES, ERROR_EXCEPTIONS,
                     WARNINGS_EXCEPTIONS)

RUN12_NAMES = ['LHCb', 'Lbcom', 'Rec', 'Phys', 'Hlt', 'Moore']

moore_2011 = Slot(
    'lhcb-hlt2011-patches',
    desc='Test slot with patches for the 2011 Moore stack',
    projects=[Project(name, 'hlt2011-patches') for name in RUN12_NAMES],
    platforms=['x86_64-slc5-gcc43-opt', 'x86_64-slc5-gcc43-dbg'],
    warning_exceptions=[r'/Boost/', r'/genreflex/'],
    build_tool='cmt',
    env=[
        "PATH=/cvmfs/lhcb.cern.ch/lib/bin/x86_64-slc5:$PATH",
        "PATH=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.17.3/Linux-x86_64/bin:$PATH"
    ])

moore_2012 = Slot(
    'lhcb-hlt2012-patches',
    desc='Test slot with patches for the 2012 Moore stack',
    projects=[Project(name, 'hlt2012-patches') for name in RUN12_NAMES],
    platforms=['x86_64-slc5-gcc46-opt', 'x86_64-slc5-gcc46-dbg'],
    warning_exceptions=[r'/Boost/', r'/genreflex/'],
    build_tool='cmt',
    env=[
        "PATH=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.17.3/Linux-x86_64/bin:$PATH"
    ])

for s in [moore_2011, moore_2012]:
    s.env.append('CMTEXTRATAGS=host-slc5')

moore_2016 = Slot(
    'lhcb-hlt2016-patches',
    desc='Test slot with patches for the 2016 Moore stack',
    projects=[
        Project(name, 'hlt2016-patches', checkout='git')
        for name in RUN12_NAMES
    ],
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    platforms=['x86_64-slc6-gcc49-opt', 'x86_64-slc6-gcc49-dbg'],
    env=[
        "PATH=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.17.3/Linux-x86_64/bin:$PATH"
    ])

RUN3_NAMES = ['Detector', 'LHCb', 'Lbcom', 'Rec', 'Allen', 'Moore']

RUN3_NAMES_wo_Detector = ['LHCb', 'Lbcom', 'Rec', 'Allen', 'Moore']

moore_2023_hlt2pp = Slot(
    'lhcb-2023-hlt2pp-patches',
    desc='Patches for the RTA/2023.08.24 stack used for 2023 HLT2 pp',
    projects=([
        Project('LCG', '103', disabled=True),
        Project('Gaudi', 'v36r16', disabled=True),
    ] + [
        Project(name, '2023-hlt2pp-patches', checkout='git')
        for name in RUN3_NAMES
    ]),
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    platforms=[
        'x86_64_v2-centos7-gcc12-opt',
        'x86_64_v2-centos7-gcc12-dbg',
        'x86_64_v2-el9-gcc12-dbg',
        'x86_64_v2-centos7-gcc12+detdesc-opt',
        'x86_64_v2-centos7-gcc12+detdesc-dbg',
    ],
    cache_entries=dict(DEFAULT_CACHE_ENTRIES),
)

moore_2023_hlt1pp = Slot(
    'lhcb-2023-hlt1pp-patches',
    desc='Patches for the RTA/2023.07.04 stack used for 2023 HLT1 pp',
    projects=([
        Project('LCG', '103', disabled=True),
        Project('Gaudi', 'v36r16', disabled=True),
        # the original stack used v36r14 but v36r16 contains only two small fixes.
    ] + [
        Project(name, '2023-hlt1pp-patches', checkout='git')
        for name in RUN3_NAMES
    ]),
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    platforms=[
        'x86_64_v2-centos7-gcc12-opt',
        'x86_64_v2-centos7-gcc12-dbg',
        'x86_64_v2-el9-gcc12-dbg',
        'x86_64_v2-centos7-gcc12+detdesc-opt',
        'x86_64_v2-centos7-gcc12+detdesc-dbg',
    ],
    cache_entries=dict(DEFAULT_CACHE_ENTRIES),
)

moore_2024_RTA2024_07_09_4 = Slot(
    'lhcb-RTA-2024-07-09-4-patches',
    desc=
    'Patches for the RTA/2024.07.09 stack used for MC production blocks 1+2',
    projects=([
        Project('LCG', '105a', disabled=True),
        Project('Gaudi', 'v38r1', disabled=True),
        Project('Detector', 'v1r36', disabled=True),
        Project('LHCb', 'v55r11p2', checkout='git'),
        Project('Lbcom', 'v35r11p2', checkout='git'),
        Project('Rec', 'v36r11p3-patches', checkout='git'),
        Project('Allen', 'v4r11p4-patches', checkout='git'),
        Project('Moore', 'v55r11p6-patches', checkout='git')
    ]),
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    platforms=[
        'x86_64_v2-el9-gcc13-opt', 'x86_64_v2-el9-gcc13-dbg',
        'x86_64_v3-el9-gcc13-opt+g', 'x86_64_v2-el9-clang16-opt',
        'x86_64_v2-el9-gcc13+detdesc-opt', 'x86_64_v2-el9-gcc13+detdesc-dbg',
        'x86_64_v3-el9-gcc13+detdesc-opt+g',
        'x86_64_v3-el9-gcc12+cuda12_1-opt+g'
    ],
    cache_entries=dict(DEFAULT_CACHE_ENTRIES),
)

moore_2024_RTA2024_08_22_4 = Slot(
    'lhcb-RTA-2024-08-22-4-patches',
    desc=
    'Patches for the RTA/2024.08.22.5 stack used for MC production blocks 5+6',
    projects=([
        Project('LCG', '105a', disabled=True),
        Project('Gaudi', 'v38r1', disabled=True),
        Project('Detector', 'v1r36', disabled=True),
        Project('LHCb', 'v55r12p1-patches', checkout='git'),
        Project('Lbcom', 'v35r12p1-patches', checkout='git'),
        Project('Rec', 'v36r12p3-patches', checkout='git'),
        Project('Allen', 'v4r12p3-patches', checkout='git'),
    ] + [Project('Moore', 'v55r12p5-patches', checkout='git')]),
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    platforms=[
        'x86_64_v2-el9-gcc13-opt', 'x86_64_v2-el9-gcc13-dbg',
        'x86_64_v3-el9-gcc13-opt+g', 'x86_64_v2-el9-clang16-opt',
        'x86_64_v2-el9-gcc13+detdesc-opt', 'x86_64_v2-el9-gcc13+detdesc-dbg',
        'x86_64_v3-el9-gcc13+detdesc-opt+g',
        'x86_64_v3-el9-gcc12+cuda12_1-opt+g'
    ],
    cache_entries=dict(DEFAULT_CACHE_ENTRIES),
)

moore_2024_RTA2024_09_29_3 = Slot(
    'lhcb-RTA-2024-09-29-3-patches',
    desc=
    'Patches for the RTA/2024.09.29.3 stack used for MC production blocks 7+8',
    projects=([
        Project('LCG', '105a', disabled=True),
        Project('Gaudi', 'v38r1', disabled=True),
        Project('Detector', 'v1r36', disabled=True),
        Project('LHCb', 'v55r13p1-patches', checkout='git'),
        Project('Lbcom', 'v35r13p1-patches', checkout='git'),
        Project('Rec', 'v36r13p1-patches', checkout='git'),
        Project('Allen', 'v4r13p3-patches', checkout='git'),
    ] + [Project('Moore', 'v55r13p4-patches', checkout='git')]),
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    platforms=[
        'x86_64_v2-el9-gcc13-opt', 'x86_64_v2-el9-gcc13-dbg',
        'x86_64_v3-el9-gcc13-opt+g', 'x86_64_v2-el9-clang16-opt',
        'x86_64_v2-el9-gcc13+detdesc-opt', 'x86_64_v2-el9-gcc13+detdesc-dbg',
        'x86_64_v3-el9-gcc13+detdesc-opt+g',
        'x86_64_v3-el9-gcc12+cuda12_1-opt+g'
    ],
    cache_entries=dict(DEFAULT_CACHE_ENTRIES),
)

moore_2024_MC_pp_ref_production = Slot(
    'lhcb-2024-mc-ppref-patches',
    desc='Patches for 2024 pp reference run MC production',
    projects=([
        Project('LCG', '105a', disabled=True),
        Project('Gaudi', 'v38r1p1', disabled=True),
        Project('Detector', 'v1r36', disabled=True),
        Project('LHCb', 'v55r14-patches', checkout='git'),
        Project('Lbcom', 'v35r14-patches', checkout='git'),
        Project('Rec', 'v36r14-patches', checkout='git'),
        Project('Allen', 'v4r14-patches', checkout='git'),
    ] + [Project('Moore', 'v55r14p2-patches', checkout='git')]),
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    platforms=[
        'x86_64_v2-el9-gcc13-opt', 'x86_64_v2-el9-gcc13-dbg',
        'x86_64_v3-el9-gcc13-opt+g', 'x86_64_v2-el9-clang16-opt',
        'x86_64_v2-el9-gcc13+detdesc-opt', 'x86_64_v2-el9-gcc13+detdesc-dbg',
        'x86_64_v3-el9-gcc13+detdesc-opt+g',
        'x86_64_v3-el9-gcc12+cuda12_1-opt+g'
    ],
    cache_entries=dict(DEFAULT_CACHE_ENTRIES),
)

moore_2024_MC_PbPb_production = Slot(
    'lhcb-2024-mc-pbpb-patches',
    desc='Patches for 2024 PbPb MC production',
    projects=([
        Project('LCG', '105a', disabled=True),
        Project('Gaudi', 'v38r1p1', disabled=True),
        Project('Detector', 'v1r36', disabled=True),
        Project('LHCb', 'v55r16p1-patches', checkout='git'),
        Project('Lbcom', 'v35r16p1-patches', checkout='git'),
        Project('Rec', 'v36r16p1-patches', checkout='git'),
        Project('Allen', 'v4r16p4-patches', checkout='git'),
    ] + [Project('Moore', 'v55r16p5-patches', checkout='git')]),
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    platforms=[
        'x86_64_v2-el9-gcc13-opt', 'x86_64_v2-el9-gcc13-dbg',
        'x86_64_v3-el9-gcc13-opt+g', 'x86_64_v2-el9-clang16-opt',
        'x86_64_v2-el9-gcc13+detdesc-opt', 'x86_64_v2-el9-gcc13+detdesc-dbg',
        'x86_64_v3-el9-gcc13+detdesc-opt+g',
        'x86_64_v3-el9-gcc12+cuda12_1-opt+g'
    ],
    cache_entries=dict(DEFAULT_CACHE_ENTRIES),
)

slots = [
    moore_2011, moore_2012, moore_2016, moore_2023_hlt1pp, moore_2023_hlt2pp,
    moore_2024_RTA2024_07_09_4, moore_2024_RTA2024_08_22_4,
    moore_2024_RTA2024_09_29_3, moore_2024_MC_pp_ref_production,
    moore_2024_MC_PbPb_production
]

# Common slot settings
for slot in slots:
    slot.projects.append(DBASE(packages=[Package('PRConfig', 'HEAD')]))
    for p in slot.projects:
        if not p.disabled:
            p.checkout_opts['merges'] = 'all'

    if hasattr(slot, "Allen"):
        node_overrides = {
            f"Allen/{platform}": "cuda-tests"
            for platform in slot.platforms if "cuda" in platform
        }
        if node_overrides:
            slot.metadata["node_overrides"] = node_overrides
