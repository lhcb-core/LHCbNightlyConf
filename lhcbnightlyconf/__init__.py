###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, print_function

try:
    import lb.nightly.configuration
except ImportError:
    import sys

    import LbNightlyTools.Configuration
    sys.modules["lb.nightly.configuration"] = LbNightlyTools.Configuration
    if sys.version_info < (3, 0):

        class _mock:
            pass

        sys.modules["lb.nightly"] = _mock()
        sys.modules["lb.nightly"].configuration = sys.modules[
            "lb.nightly.configuration"]
        sys.modules["lb"] = _mock()
        sys.modules["lb"].nightly = sys.modules["lb.nightly"]

from itertools import chain

from . import (G4R10, Boole, DaVinciTests, Gauss, Main, Master, MooreLegacy,
               Patches, Reco14, Reco15, Release, Run5, StrippingTests,
               Temporary, lcg_nightlies)

slots = []
slots.extend(
    chain(
        Master.slots,
        Main.slots,
        Boole.slots,
        DaVinciTests.slots,
        G4R10.slots,
        Gauss.slots,
        lcg_nightlies.slots,
        MooreLegacy.slots,
        Patches.slots,
        Reco14.slots,
        Reco15.slots,
        StrippingTests.slots,
        Temporary.slots,
        Release.slots,
        Run5.slots,
    ))

# Flag slots that can be used as models for /ci-test
for slot in slots:
    if slot.name in (
            "lhcb-master",
            "lhcb-sim10",
    ) or slot.name.endswith("-patches"):
        slot.metadata["ci_test_model"] = True
