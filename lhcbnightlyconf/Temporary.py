###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from lb.nightly.configuration import DBASE, Package, Project, Slot

from .Main import create_head_slot
from .Patches import preparePatchesSlot

from .Common import (  # isort: skip
    DEFAULT_CACHE_ENTRIES, DEFAULT_ENV, WARNINGS_EXCEPTIONS, ERROR_EXCEPTIONS,
    DEFAULT_GAUDI_RELEASED, DEFAULT_GAUDI_VERSION,
)

slots = []

slots.append(
    Slot(
        'new-nightly-test',
        disabled=True,
        desc=
        'minimal slot used to test the new nightly builds system, see <a href="https://its.cern.ch/jira/browse/LBCORE-1702">LBCORE-1702</a>',
        projects=[
            Project('LCG', '105c', disabled=True),
            DBASE(packages=[Package("PRConfig", "HEAD")]),
            Project('Gaudi', 'HEAD'),
            Project('Detector', 'HEAD'),
            Project('LHCb', 'HEAD'),
        ],
        platforms=[
            "x86_64_v2-el9-gcc13-opt", "x86_64_v2-el9-gcc13-dbg",
            "armv8.1_a-el9-gcc13-opt"
        ],
        env=list(DEFAULT_ENV),
        warning_exceptions=list(WARNINGS_EXCEPTIONS),
        error_exceptions=list(ERROR_EXCEPTIONS),
        cache_entries=dict(DEFAULT_CACHE_ENTRIES),
    ))

slots.append(
    Slot(
        'test-cuda',
        disabled=True,
        desc=
        'minimal slot used to test the new nightly builds system, see <a href="https://its.cern.ch/jira/browse/LBCORE-1702">LBCORE-1702</a>',
        projects=[
            Project('LCG', '105c', disabled=True),
            DBASE(packages=[Package("PRConfig", "HEAD")]),
            Project('Allen', 'HEAD'),
        ],
        platforms=[
            "x86_64_v2-el9-gcc13-opt", "x86_64_v3-el9-gcc13+cuda12_4-opt+g"
        ],
        env=list(DEFAULT_ENV),
        warning_exceptions=list(WARNINGS_EXCEPTIONS),
        error_exceptions=list(ERROR_EXCEPTIONS),
        cache_entries=dict(DEFAULT_CACHE_ENTRIES),
        metadata={
            "node_overrides": {
                "Allen/x86_64_v3-el9-gcc13+cuda12_4-opt+g": "cuda-tests"
            }
        }))

slots.append(preparePatchesSlot("2016", "test-2016"))
slots[-1].disabled = True
for project in slots[-1].projects:
    project.disabled = project.name not in ("LHCb", "Lbcom", "DBASE", "PARAM")

slots.append(
    Slot(
        'test-hlt2012',
        disabled=True,
        desc='Test slot with patches for the 2012 Moore stack',
        projects=[
            Project(name, 'hlt2012-patches') for name in ("LHCb", "Lbcom")
        ],
        platforms=['x86_64-slc5-gcc46-opt', 'x86_64-slc5-gcc46-dbg'],
        warning_exceptions=[r'/Boost/', r'/genreflex/'],
        build_tool='cmt',
        env=[
            "PATH=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.17.3/Linux-x86_64/bin:$PATH",
            "CMTEXTRATAGS=host-slc5",
        ]))

slots.append(
    create_head_slot(
        "nightly-builds-test",
        LCG="105c",
        projects=["LCG", "Gaudi", "Detector", "LHCb"],
        platforms=[
            "x86_64_v2-centos7-gcc11-opt",
            "x86_64_v2-el9-gcc12-opt",
            "armv8.1_a-centos7-gcc11-opt",
        ],
    ))
slots[-1].disabled = True

slots.append(
    Slot(
        "quarantine-orwell",
        desc="Special slot to build <em>only</em> Orwell, until it is fixed",
        projects=[
            Project('Rec', 'v31r2', disabled=True),
            Project("Orwell", "HEAD")
        ],
        platforms=["x86_64-centos7-gcc9-opt", "x86_64-centos7-gcc9+py3-opt"]))

slots.append(
    Slot(
        "quarantine-Lovell",
        desc="Special slot to build <em>only</em> Lovell, until it is fixed",
        projects=[
            Project('Gaudi', 'v34r1', disabled=True),
            Project("Lovell", "HEAD")
        ],
        platforms=["x86_64-centos7-gcc9-opt", "x86_64-centos7-gcc9+py3-opt"]))
