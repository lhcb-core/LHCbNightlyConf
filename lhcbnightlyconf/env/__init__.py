###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from copy import deepcopy
from hashlib import sha256

import yaml

try:
    from importlib.resources import open_text
except ImportError:  # Python 2
    from importlib_resources import open_text

CONDA_ARCHS = ["linux-64", "linux-aarch64"]

envs = {}

environment = {}  # this is not really used but helps linters
for name in ["legacy"]:
    envs[name] = {}
    for conda_arch in CONDA_ARCHS:
        # FIXME: luigi is not available for ARM (but not really needed)
        if name == "scheduler_env" and conda_arch == "linux-aarch64":
            continue
        with open_text(__name__, "{}-{}.yml".format(name,
                                                    conda_arch)) as envfile:
            envs[name][conda_arch] = yaml.safe_load(envfile)
del name
del conda_arch
globals().update(envs)


def get_environment(overrides=[], env=environment):
    """
    Helper function updating the default environment in the conda format.
    It looks for a package to be updated amongst those installed directly
    from conda, and if not found, it checks the requirements
    installed with pip. If the requested package is not found, it is added
    to the list of packages.

    :param override: list of dictionaries. Each dictionary must contain
    keys named 'package' and 'version' (otherwise ValueError is raised).
    Optionally, it may contain 'pip' key which, when evaluated to True,
    adds the package to the list of packages installed with pip (by default
    new packages will be added to the list installed with conda)
    :param env (optional): dictionary containing the environment in conda
    format. By default, the default nightlies environment is used.

    Returns the copy of the dictionary with the updated environment
    in the format of the conda environment.yml file.
    It can be assigned to the slot.metadata["environment"].
    """
    myenv = deepcopy(env)
    try:
        conda_deps = [x for x in myenv["dependencies"] if not isinstance(x, dict)] # yapf: disable
    except KeyError:
        raise ValueError("Provided environment is incorrect")
    pip_deps = next(
        (x for x in myenv["dependencies"] if isinstance(x, dict) and "pip" in x), None
    ) # yapf: disable

    for override in overrides:
        if "package" not in override or "version" not in override:
            raise ValueError(
                "incorrect format of the dictionary to upgrade the version of the package"
            )
        if any(override["package"] in dep for dep in conda_deps):
            conda_deps = [
                "{package}={version}".format(**override)
                if override["package"] in dep
                else dep
                for dep in conda_deps
            ] # yapf: disable
        elif pip_deps and any(override["package"] in dep for dep in pip_deps["pip"]): # yapf: disable
            if override["version"].startswith("git"):
                pip_deps["pip"] = [
                    override["version"] if override["package"] in dep else dep
                    for dep in pip_deps["pip"]
                ] # yapf: disable
            else:
                pip_deps["pip"] = [
                    "{package}=={version}".format(**override)
                    if override["package"] in dep
                    else dep
                    for dep in pip_deps["pip"]
                ] # yapf: disable
        else:
            if override.get("pip", False):
                if not pip_deps:
                    pip_deps = {"pip": []}
                if override["version"].startswith("git"):
                    pip_deps["pip"].append(override["version"])
                else:
                    pip_deps["pip"].append(
                        "{package}=={version}".format(**override)
                    ) # yapf: disable
            else:
                conda_deps.append("{package}={version}".format(**override)) # yapf: disable
    myenv["dependencies"] = conda_deps
    if pip_deps:
        myenv["dependencies"].append(pip_deps)
    return myenv


def environment_hash(environment):
    """
    Return a hash of the environment name.
    """
    # ignore name and prefix in the environment
    env = dict(environment)
    for k in ("name", "prefix"):
        if k in env:
            del env[k]
    return sha256(yaml.safe_dump(env).encode("utf-8")).hexdigest()
