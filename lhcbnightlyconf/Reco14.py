# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from itertools import starmap

from lb.nightly.configuration import DBASE, Package, Project, Slot

reco14 = Slot(
    'lhcb-reco14-patches',
    desc='preparation of a patched version of Brunel for Reco14 '
    '(see <a href="https://its.cern.ch/jira/browse/LHCBGAUSS-1065">LHCBGAUSS-1065</a>)',
    projects=starmap(Project, [('LHCb', 'reco14-patches'),
                               ('Lbcom', 'reco14-patches'),
                               ('Rec', 'reco14-patches'),
                               ('Brunel', 'reco14-patches'),
                               ('Phys', '2013-patches'),
                               ('Analysis', '2013-patches'),
                               ('Stripping', '2013-patches'),
                               ('DaVinci', '2013-patches')]),
    platforms=['x86_64-slc5-gcc46-opt', 'x86_64-slc5-gcc46-dbg'],
    build_tool='cmt',
    env=[
        "PATH=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.17.3/Linux-x86_64/bin:$PATH"
    ])

for p in reco14.projects:
    p.checkout_opts['merges'] = 'all'

reco14.projects.append(DBASE(packages=[Package('PRConfig', 'HEAD')]))

slots = [reco14]
