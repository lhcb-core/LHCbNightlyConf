# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Conguration of Geant4 relase 10 slots
"""
# datetime for rebuildAlways temporary hack
import datetime

from lb.nightly.configuration import DBASE, PARAM, Package, Project, Slot

from .Common import ERROR_EXCEPTIONS, WARNINGS_EXCEPTIONS

cmakeEnv = [('CMAKE_PREFIX_PATH=' + ':'.join([
    '/cvmfs/lhcbdev.cern.ch/lib/lcg/releases',
    '/cvmfs/lhcb.cern.ch/lib/lcg/releases',
    '/cvmfs/sft.cern.ch/lcg/releases',
    '${CMAKE_PREFIX_PATH}',
]))]

g4dev = Slot(
    name='lhcb-g4-dev',
    projects=[
        Project('LCG', '102b', disabled=True),
        Project('Geant4', 'HEAD'),
        Project('Gaudi', 'v36r9p1', disabled=True),
        Project('LHCb', 'v53r9p4', disabled=True),
        Project('Run2Support', 'v2r2p3', disabled=True),
        Project('Gauss', 'Sim10', checkout=('git', {
            'merges': 'all'
        })),
        DBASE(packages=[Package('PRConfig', 'HEAD')])
    ],
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    build_tool='cmake')

g4dev.desc = 'Testing new Geant4 release for LHCb Simulation'
g4dev.cache_entries = {
    'CMAKE_USE_CCACHE': True,
    'GAUDI_LEGACY_CMAKE_SUPPORT': True
}
g4dev.platforms = [
    'x86_64_v2-centos7-gcc11-opt', 'x86_64_v2-centos7-gcc11-dbg'
]
g4dev.build_tool = 'cmake'
g4dev.env = cmakeEnv
# Temp hack to trigger slot rebuild
g4dev.metadata["rebuildAlways"] = str(datetime.date.today())

slots = []
