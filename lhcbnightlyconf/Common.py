# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Common settings
'''
import sys

from lb.nightly.configuration import Project

ALL_PROJECTS = [
    'LCG',
    'Gaudi',
    'Detector',
    'LHCb',
    'Lbcom',
    'Rec',
    'Allen',
    'Moore',
    'Alignment',
    'Online',
    'MooreOnline',
    'Panoptes',
    'DaVinci',
    'LHCbIntegrationTests',
    'Run2Support',
    'Boole',
    'Geant4',
    'GaussinoExtLibs',
    'Gaussino',
    'Gauss',
]

DEFAULT_DBASE_PACKAGES = [
    ('PRConfig', 'HEAD'),
]

# set of production platforms for the latest LCG on master slots
MASTER_CITEST_PLATFORMS = [
    # gcc13 DD4hep
    'x86_64_v2-el9-gcc13-opt',
    'x86_64_v2-el9-gcc13-dbg',
    'x86_64_v3-el9-gcc13-opt+g',
    # gcc13 detdesc
    'x86_64_v2-el9-gcc13+detdesc-dbg',
    'x86_64_v3-el9-gcc13+detdesc-opt+g',
    # clang compiler
    'x86_64_v2-el9-clang16-opt',
    # arm
    "armv8.1_a-el9-gcc13-opt",
    # special targets
    'x86_64_v3-el9-gcc13+cuda12_4-opt+g',
]

MASTER_PLATFORMS = [
    # gcc13 DD4hep
    'x86_64_v2-el9-gcc13-opt',
    'x86_64_v2-el9-gcc13-dbg',
    'x86_64_v3-el9-gcc13-opt+g',
    # gcc13 detdesc
    'x86_64_v2-el9-gcc13+detdesc-opt',
    'x86_64_v2-el9-gcc13+detdesc-dbg',
    'x86_64_v3-el9-gcc13+detdesc-opt+g',
    # clang compiler
    'x86_64_v2-el9-clang16-opt',
    'x86_64_v2-el9-clang16-dbg',
    'x86_64_v2-el9-clang16+detdesc-opt',
    # arm
    "armv8.1_a-el9-gcc13-opt",
    "armv8.1_a-el9-gcc13-dbg",
    # special targets
    'x86_64_v4+vecwid256-el9-gcc13-opt',
    'x86_64_v3-el9-gcc13+cuda12_4-opt+g',
]

# - on run2 slots
PLATFORMS_RUN2 = [
    # gcc13
    'x86_64_v2-el9-gcc13-opt',
    'x86_64_v2-el9-gcc13-dbg',
    # clang16
    'x86_64_v2-el9-clang16-opt',
    'x86_64_v2-el9-clang16-dbg',
]

# Gaudi version to use in lhcb-master, lhcb-head, lhcb-run2-patches.
# It should be an existing tag (deployed on cvmfs or not).
DEFAULT_GAUDI_VERSION = "v39r3"
DEFAULT_GAUDI_RELEASED = False  # controls whether we build/test the above version

DEFAULT_SEARCH_PATH = [
    '/cvmfs/lhcbdev.cern.ch/lib/lcg/releases',
    '${CMAKE_PREFIX_PATH}',
    '${CMTPROJECTPATH}',
    '/cvmfs/sft.cern.ch/lcg/releases',
]

DEFAULT_ENV = [
    'CMAKE_PREFIX_PATH=' + ':'.join(DEFAULT_SEARCH_PATH),
    'PYTEST_DISABLE_FIXTURES_REQUIRED=1',
]

# extra CMake cache variables that are (currently) propagated to nightly slots
# relevant for the upgrade.
DEFAULT_CACHE_ENTRIES = {
    # Special settings required for new style CMake projects
    'GAUDI_LEGACY_CMAKE_SUPPORT': True,
    # Do not skip tests that have a failed prerequisite test
    'QMTEST_DISABLE_FIXTURES_REQUIRED': True,
    # Enforce CMake policy CMP0167 <https://cmake.org/cmake/help/latest/policy/CMP0167.html> to NEW
    # to be removed once all projects that can be built with CMake 3.30 have been adapted
    "CMAKE_POLICY_DEFAULT_CMP0167": "NEW",
}

WARNINGS_EXCEPTIONS = [
    r'.*/(Boost|ROOT)/.*',
    # CMake developer warning.
    # this is to ignore CMake (>=3.15) warning about "project(Project)"
    # missing in the top level CMakeLists.txt
    r'CMake Warning \(dev\) in CMakeLists\.txt\:',
    r'This warning is for project developers',
    # This is an unavoidable warning when mixing new style CMake projects with
    # legacy ones
    r'CMakeLists.txt does not contain gaudi_project',
    # Suppressions for CMake 3.19 with old Vc, LBCORE-1992
    (r'CMake Deprecation Warning at .*/cmake/Vc/VcMacros.cmake.*'
     r' \(cmake_minimum_required\):'),
    # Temporary suppressions for CMake 3.19
    (r'CMake Deprecation Warning at .*CMakeLists.txt.*'
     r' \((CMAKE_MINIMUM_REQUIRED|cmake_minimum_required)\):'),
    # Suppress message from EvtGen's cmake, identified as warning:
    # -- EvtGen: Customising warning/optimise/debug flags for each build type
    r'.*EvtGen: Customising.*',
    # Suppress pragma message from TBB (identified as warning in Gaudi)
    r'.*TBB Warning: tbb/task_scheduler_init\.h is deprecated\..*',
    # Suppress ssh key warnings, introduced in Moore with
    # https://gitlab.cern.ch/lhcb/Moore/-/merge_requests/1529
    r'Warning: Permanently added the ECDSA host key for IP address.*',
    # Spurious warning from CMake when a policy has to be updated
    r"set the policy and suppress this warning",
]

ERROR_EXCEPTIONS = [
    r'Error\.cc',  # hide false positive from build of Delphes
    r'-Wno-error',  # hide false positive from googletest building with -Wno-error=dangling-else
]

DEFAULT_REQUIRED_EXTERNALS = [
    "AIDA",
    "Boost",
    "CLHEP",
    "CppUnit",
    "eigen",
    # "expat",
    "fastjet",
    "fftw",
    "graphviz",
    "GSL",
    "HepMC",
    "HepPDT",
    "libgit2",
    "libunwind",
    "Python",
    "RELAX",
    "ROOT",
    "sqlite",
    "tbb",
    "vdt",
    "XercesC",
    # Not present in aarch64 builds
    # "xqilla",
    "xrootd",
]

if "LbNightlyTools.Configuration" in sys.modules:

    class LCGToolchains(Project):
        '''
        Special custom project to add 'lcg-toolchains' to a slot.
        '''
        name = 'lcg-toolchains'
        __checkout__ = 'git'
        ignore_slot_build_tool = True
        build_tool = 'no_build'

        def __init__(self, name, version, **kwargs):
            Project.__init__(self, name, version, **kwargs)
            self.checkout_opts = {
                'url': 'https://gitlab.cern.ch/lhcb-core/lcg-toolchains.git'
            }
            self.no_test = True
else:

    class LCGToolchains(Project):
        '''
        Special custom project to add 'lcg-toolchains' to a slot.
        '''

        def __init__(self, version):
            Project.__init__(
                self,
                'lcg-toolchains',
                version,
                ignore_slot_build_tool=True,
                build_tool='no_build',
                checkout='git',
                checkout_opts={
                    'url':
                    'https://gitlab.cern.ch/lhcb-core/lcg-toolchains.git'
                })
            self.no_test = True
