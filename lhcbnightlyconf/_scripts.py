###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import multiprocessing
import os
from pathlib import Path
from typing import Tuple, Union

import click
import requests
import yaml

from lhcbnightlyconf import slots


@click.command()
@click.option(
    "--output",
    default="-",
    help="Name of the JSON output file, by default use stdout (AKA '-')",
    type=click.File("w"))
@click.option(
    "--commit",
    default=os.environ.get("CI_COMMIT_SHA"),
    help="Commit id to record in the JSON output [default from $CI_COMMIT_SHA]"
)
def list_slots(output, commit):
    """
    Print the list of slot names as a JSON object
    """
    json.dump({
        'slots': sorted([slot.name for slot in slots]),
        'commit': commit
    },
              output,
              indent=2)


def get_unique_environments():
    """
    Return a dictionary of unique environments with hashes as the keys.
    """
    from lhcbnightlyconf.env import environment_hash, envs

    def all_envs():
        for slot in slots:
            if "environment" in slot.metadata:
                for arch in slot.metadata["environment"]:
                    yield slot.metadata["environment"][arch]
        for name in envs:
            for arch in envs[name]:
                yield envs[name][arch]

    return {environment_hash(env): env for env in all_envs()}


@click.command()
@click.option(
    "--output",
    default="-",
    help="Name of the YAML output file, by default use stdout (AKA '-')",
    type=click.File("w"))
def list_environments(output):
    """
    List all needed Conda environments for the defined slots as a YAML mapping.
    """
    yaml.safe_dump(get_unique_environments(), output)


@click.command()
@click.option(
    "--output",
    default="-",
    help="Name of the YAML output file, by default use stdout (AKA '-')",
    type=click.File("w"))
def env_hashes(output):
    """
    List the hashes of all environments needed.
    """
    from lhcbnightlyconf.env import environment_hash, envs

    hashes = {
        slot.name: {
            arch: environment_hash(env)
            for arch, env in slot.metadata["environment"].items()
        }
        for slot in slots if "environment" in slot.metadata
    }
    hashes.update((k, {arch: environment_hash(env)
                       for arch, env in e.items()}) for k, e in envs.items())

    yaml.safe_dump(hashes, output)


@click.command()
@click.option(
    "--dest-dir",
    default=".",
    help=
    "Destination directory for the environment files [default to current dir]",
    type=click.Path(file_okay=False, dir_okay=True, writable=True))
@click.option(
    "--as-env",
    "format",
    flag_value="yaml",
    default=True,
    help="output environment configuration for 'conda env create' (YAML)")
@click.option(
    "--as-list",
    "format",
    flag_value="list",
    help="output environment configuration for 'conda create' (flat list)")
def dump_envs(dest_dir, format):
    """
    Dump the environment files to the given directory.
    """
    if not os.path.isdir(dest_dir):
        os.makedirs(dest_dir)

    extension, formatter = {
        "yaml": (".yml", yaml.safe_dump),
        "list": (".txt", lambda v: "\n".join(l for l in v.get("dependencies", []) if isinstance(l, str)))
    }[format]
    for k, v in get_unique_environments().items():
        with open(os.path.join(dest_dir, k + extension), "w") as f:
            f.write(formatter(v))


def _test_env(environment: dict):
    """
    Run conda environment dry-run test and return a
    subprocess.CompletedProcess instance.
    """
    from itertools import chain
    from subprocess import run
    from tempfile import TemporaryDirectory

    cmdline = ["mamba", "create", "--dry-run", "--quiet"]
    channels = environment.get("channels", [])
    if "nodefaults" in channels:
        cmdline.append("--override-channels")
        channels.remove("nodefaults")
    if channels:
        cmdline.extend(
            chain.from_iterable(("-c", channel) for channel in channels))

    # note we are ignoring the pip section of the environment
    cmdline.extend(
        dep for dep in environment.get("dependencies", [])
        if isinstance(dep, str))
    conda_subdir = environment.get("variables", {}).get("CONDA_SUBDIR")

    with TemporaryDirectory() as tmpdir:
        cmdline.extend(("--prefix", os.path.join(tmpdir, "prefix")))
        env = dict(os.environ)
        env["CONDA_PKGS_DIRS"] = os.path.join(tmpdir, "conda_pkgs")
        if conda_subdir:
            env["CONDA_SUBDIR"] = conda_subdir
        return run(cmdline, capture_output=True, env=env)


class EnvTester:
    """
    helper to run the test via multiprocessing.Pool
    """

    def __init__(self, deployment_root: Union[Path, str, None] = None):
        """
        When deployment_root is specified, it is used to see if the environment is already there,
        in which case we can assume it is good.
        """
        self.deployment_root = Path(
            deployment_root) if deployment_root else None

    def __call__(self, args: Tuple[str, dict]):
        from subprocess import CompletedProcess
        name, env = args
        if self.deployment_root and (self.deployment_root / name).is_dir():
            # we found the environment directory already deployed, it means it is valid,
            # so we just pretend to run the test task and claim it succeeded
            return name, CompletedProcess(
                [], 0, b"environment found in deployment_root")
        return name, _test_env(env)


@click.command()
@click.option(
    "--file", "-f", "env_file", help="YAML file to read", type=click.File("r"))
@click.option(
    "--jobs",
    "-j",
    help="number of parallel jobs to run (default: use all cores)",
    type=int)
@click.option(
    "--verbose",
    "-v",
    help="print full conda output",
    default=False,
    is_flag=True)
@click.option(
    "--deployment-root",
    "-d",
    help=
    "directory where the environment are finally deployed (used as cache to speed up checks)",
    type=click.Path(exists=True, file_okay=False, path_type=Path))
@click.argument("environment_ids", required=False, nargs=-1)
def test_envs(environment_ids, env_file, jobs, verbose, deployment_root):
    """
    Test the given environments.

    If no environment ids are given, all environments are tested,
    unless a YAML file is given, in which case the file is the only
    environment to test.

    If environment ids are given, only the given environments are
    tested plus optionally the specified YAML file.
    """
    all_envs = get_unique_environments()
    if environment_ids:  # test only the given envs
        try:
            envs = {key: all_envs[key] for key in environment_ids}
        except KeyError as e:
            raise click.BadParameter(str(e))
    elif env_file:  # no envs given, but a file, test the file only
        envs = {}
    else:  # no envs nor file given, test all envs
        envs = all_envs

    if env_file:
        envs[env_file.name] = yaml.safe_load(env_file)

    if not envs:
        raise click.BadParameter("No environments to test")

    if jobs is None:
        jobs = multiprocessing.cpu_count()
    jobs = min(jobs, len(envs))

    all_ok = True
    with multiprocessing.Pool(processes=jobs) as pool:
        click.secho(
            "Testing environments:\n - {}".format('\n - '.join(envs)),
            fg="blue")
        click.secho(f"Running {jobs} jobs in parallel", fg="blue")
        for name, result in pool.imap_unordered(
                EnvTester(deployment_root), envs.items()):
            if verbose:
                click.secho(f"stdout for {name}", fg="blue")
                click.echo(result.stdout.decode(errors="replace").strip())
            if result.returncode == 0:
                click.secho(f"Environment {name} is valid", fg="green")
            else:
                click.secho(f"Environment {name} contains errors:", fg="red")
                click.echo(result.stderr.decode(errors="replace").strip())
                all_ok = False

    if not all_ok:
        exit(1)


class EnvDeployer:
    """
    Helper to trigger the deployment of a given environment.
    """

    def __init__(self, base_url, auth_token):
        self.base_url = base_url.rstrip("/")
        self.auth_token = auth_token

    def __call__(self, args: Tuple[str, dict]) -> Tuple[str, dict]:
        key, env = args
        url = f"{self.base_url}/{key}/"
        data = yaml.dump(env)
        return key, requests.put(
            url,
            headers={
                "Authorization": f"Bearer {self.auth_token}"
            },
            params={
                "conda_subdir":
                env.get("variables", {}).get("CONDA_SUBDIR", "linux-64")
            },
            data=data).json()


@click.command()
@click.argument("base_url")
@click.option(
    "--auth-token",
    help=
    "authentication token for the target url [default taken from $CONDA_ENV_DEPLOYMENT_TOKEN]"
)
@click.option(
    "--jobs",
    "-j",
    help="number of parallel jobs to run (default: use all cores)",
    type=int)
def deploy_envs(base_url, auth_token, jobs):
    """
    Publish the Conda environments to BASE_URL.

    Each environment is published sending the corresponding "environment.yml" file to
    <BASE_URL>/<environment_id>.
    """
    if not auth_token:
        auth_token = os.environ.get("CONDA_ENV_DEPLOYMENT_TOKEN", "")

    envs = get_unique_environments()

    if not envs:
        raise click.BadParameter("No environments to test")

    if jobs is None:
        jobs = multiprocessing.cpu_count()
    jobs = min(jobs, len(envs))

    all_ok = True
    with multiprocessing.Pool(processes=jobs) as pool:
        click.secho(
            "Deploying environments:\n - {}".format('\n - '.join(envs)),
            fg="blue")
        click.secho(f"Running {jobs} jobs in parallel", fg="blue")
        deploy = EnvDeployer(base_url, auth_token)
        for key, result in pool.imap_unordered(deploy, envs.items()):
            click.secho(
                f"{key} -> https://lhcb-core-tasks.web.cern.ch/tasks/result/{result}"
            )

    if not all_ok:
        exit(1)
