# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from itertools import starmap

from lb.nightly.configuration import DBASE, Package, Project, Slot

from .Common import ERROR_EXCEPTIONS, WARNINGS_EXCEPTIONS

reco15 = Slot(
    'lhcb-reco15-patches',
    desc='Legacy stack for Reco15 (Brunel) and 2015 Moore',
    projects=starmap(Project, [
        ('LHCb', 'reco15-patches'),
        ('Lbcom', 'reco15-patches'),
        ('Rec', 'reco15-patches'),
        ('Brunel', 'reco15-patches'),
        ('Phys', 'hlt2015-patches'),
        ('Hlt', 'hlt2015-patches'),
        ('Moore', 'hlt2015-patches'),
    ]),
    platforms=['x86_64-slc6-gcc49-opt', 'x86_64-slc6-gcc49-dbg'],
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    build_tool='cmake',
    env=[
        "PATH=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.17.3/Linux-x86_64/bin:$PATH"
    ])

for p in reco15.projects:
    p.checkout_opts['merges'] = 'all'

reco15.projects.append(DBASE(packages=[Package('PRConfig', 'HEAD')]))

slots = [reco15]
