###############################################################################
# (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from lb.nightly.configuration import DBASE, Package, Project, Slot

from .Common import ERROR_EXCEPTIONS, WARNINGS_EXCEPTIONS

digi14 = Slot(
    'lhcb-digi14-patches',
    desc=('preparation of patches for digi14 version of Boole'),
    projects=[
        Project('LHCb', 'digi14-patches'),
        Project('Lbcom', 'digi14-patches'),
        Project('Boole', 'digi14-patches'),
    ],
    platforms=['x86_64-slc6-gcc49-opt', 'x86_64-slc6-gcc49-dbg'],
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    env=[
        "PATH=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.17.3/Linux-x86_64/bin:$PATH"
    ])

for p in digi14.projects:
    p.checkout_opts['merges'] = 'all'

digi14.projects.append(DBASE(packages=[Package('PRConfig', 'HEAD')]))

slots = [digi14]
