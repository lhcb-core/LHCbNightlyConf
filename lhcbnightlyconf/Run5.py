# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from lb.nightly.configuration import Project, Slot

from .Common import (DEFAULT_CACHE_ENTRIES, DEFAULT_ENV, ERROR_EXCEPTIONS,
                     WARNINGS_EXCEPTIONS)
from .Main import LCG_DEFAULT_VERSION

run5 = Slot(
    "lhcb-run5",
    desc="build of projects' branches for U2 scoping",
    projects=[
        Project(name, version) for name, version in [
            ("LCG", LCG_DEFAULT_VERSION),
            ("Gaudi", "v39r2"),
            ("Detector", "master"),
            ("LHCb", "run5"),
            ("Lbcom", "master"),
            ("Rec", "run5"),
            ("Allen", "run5"),
            ("Moore", "run5"),
        ]
    ],
    platforms=[
        "x86_64_v2-el9-gcc13-opt", "x86_64_v2-el9-gcc13-dbg",
        "x86_64_v2-el9-clang16-opt", "x86_64_v3-el9-gcc13+cuda12_4-opt+g"
    ],
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS),
    cache_entries=dict(DEFAULT_CACHE_ENTRIES),
    env=list(DEFAULT_ENV),
)
run5.LCG.disabled = True
run5.Lbcom.no_test = True

slots = [run5]
