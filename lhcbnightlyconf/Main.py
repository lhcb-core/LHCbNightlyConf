# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Configuration for main full-stack slots.
'''
import datetime

from lb.nightly.configuration import (DBASE, PARAM, Package, Project, Slot,
                                      slot_factory)

from .Gauss import DEFAULT_GEANT4_VERSION

from .Common import (  # isort: skip
    ALL_PROJECTS, DEFAULT_CACHE_ENTRIES, DEFAULT_DBASE_PACKAGES, DEFAULT_ENV,
    DEFAULT_GAUDI_RELEASED, DEFAULT_GAUDI_VERSION, ERROR_EXCEPTIONS,
    WARNINGS_EXCEPTIONS, LCGToolchains, MASTER_PLATFORMS)

LCG_DEFAULT_VERSION = '105c'
PROJECTS_WITHOUT_TESTS = ['Lbcom', 'Panoramix', 'Geant4']

HEAD_PLATFORMS = {
    'lhcb-gaudi-head':
    MASTER_PLATFORMS,
    'lhcb-head':
    MASTER_PLATFORMS,
    'lhcb-sanitizers': [
        'x86_64_v2-el9-gcc13-dbg+asan',
        'x86_64_v2-el9-gcc13-dbg+lsan',
        'x86_64_v2-el9-gcc13-dbg+ubsan',
    ],
}


# Slots configuration
@slot_factory
def create_head_slot(name,
                     LCG=LCG_DEFAULT_VERSION,
                     projects=ALL_PROJECTS,
                     platforms=None):
    slot = Slot(
        name,
        desc='head of everything against Gaudi/HEAD',
        projects=[Project(pname, "HEAD") for pname in projects],
        platforms=list(
            HEAD_PLATFORMS.get(name) if platforms is None else platforms),
        env=list(DEFAULT_ENV),
        warning_exceptions=list(WARNINGS_EXCEPTIONS),
        error_exceptions=list(ERROR_EXCEPTIONS),
        cache_entries=dict(DEFAULT_CACHE_ENTRIES))
    if hasattr(slot, 'LCG'):
        slot.desc += (
            ' and <a href="http://lcginfo.cern.ch/release/{LCG_version}/" '
            'target="_blank">LCG_{LCG_version}</a>').format(LCG_version=LCG)
        slot.LCG.version = LCG
        slot.LCG.disabled = True
    if hasattr(slot, 'Geant4'):
        slot.Geant4.with_shared = True
    if hasattr(slot, 'Gaussino'):
        slot.cache_entries['GEANT4_BUILD_MULTITHREADED'] = True
        slot.Gaussino.checkout_opts['url'] = (
            'https://gitlab.cern.ch/Gaussino/Gaussino.git')
    if hasattr(slot, 'GaussinoExtLibs'):
        slot.GaussinoExtLibs.checkout_opts['url'] = (
            'https://gitlab.cern.ch/Gaussino/gaussinoextlibs.git')
    if hasattr(slot, 'DD4hepDDG4'):
        slot.DD4hepDDG4.checkout_opts['url'] = (
            'https://gitlab.cern.ch/Gaussino/DD4hepDDG4.git')
    for pname in PROJECTS_WITHOUT_TESTS:
        if hasattr(slot, pname):
            getattr(slot, pname).no_test = True
    slot.cache_entries['GEANT4_TAG'] = 'lhcb/v10.6.2-branch'
    slot.cache_entries['GEANT4FILES_VERSION'] = 'v106r1'
    slot.env.append(
        'CMAKE_PREFIX_PATH=${PWD}/build/Gaudi/cmake:${CMAKE_PREFIX_PATH}')

    slot.projects.extend([
        DBASE(packages=[
            Package(name, version) for name, version in DEFAULT_DBASE_PACKAGES
        ]),
        PARAM(packages=[
            Package('TMVAWeights', 'HEAD'),
            Package('ParamFiles', 'HEAD'),
        ])
    ])

    if hasattr(slot, "Allen"):
        node_overrides = {
            f"Allen/{platform}": "cuda-tests"
            for platform in slot.platforms if "cuda" in platform
        }
        if node_overrides:
            slot.metadata["node_overrides"] = node_overrides

    return slot


slots = []

gaudi_head = create_head_slot('lhcb-gaudi-head')
gaudi_head.cache_entries["GAUDI_TEST_PUBLIC_HEADERS_BUILD"] = True
slots.append(gaudi_head)

head = create_head_slot('lhcb-head', LCG="106c")
head.cache_entries["LCG_LAYER"] = "LHCB_8"
head.Gaudi.version = DEFAULT_GAUDI_VERSION
head.Gaudi.disabled = DEFAULT_GAUDI_RELEASED
head.desc = head.desc.replace(
    'Gaudi/HEAD', ('<a href="https://gitlab.cern.ch/gaudi/Gaudi/-/tree/{0}" ' +
                   'target="_blank">Gaudi/{0}</a>').format(head.Gaudi.version))
slots.append(head)

# Slot for sanitizer tests
SAN_PROJECTS = [
    'LCG',
    'Gaudi',
    'Detector',
    'LHCb',
    'Lbcom',
    'Rec',
    'Boole',
    'Online',
    'Allen',
    'Moore',
    'DaVinci',
    'Alignment',
    'MooreOnline',
    'Panoptes',
    'Geant4',
    'Run2Support',
    'GaussinoExtLibs',
    'Gaussino',
    'Gauss',
]
san = create_head_slot('lhcb-sanitizers', projects=SAN_PROJECTS)
san.Geant4.version = DEFAULT_GEANT4_VERSION
san.Geant4.disabled = True
san.Gaudi.checkout = ('git', {'merges': 'all'})

san.desc = san.desc.replace(
    'Gaudi/HEAD', ('<a href="https://gitlab.cern.ch/gaudi/Gaudi/-/tree/{0}" ' +
                   'target="_blank">Gaudi/{0}</a>').format(san.Gaudi.version))
san.desc = san.desc + (
    ' built with '
    '<a href="http://clang.llvm.org/docs/AddressSanitizer.html" target="_blank">address</a>, '
    '<a href="http://clang.llvm.org/docs/LeakSanitizer.html" target="_blank">leak</a> and '
    '<a href="http://clang.llvm.org/docs/UndefinedBehaviorSanitizer.html" target="_blank">undefined behaviour</a> '
    'sanitizers enabled')

# build lhcb-sanitizers only on Sunday
san.disabled = (datetime.date.today().isoweekday() % 7) != 0
slots.append(san)
