# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from itertools import starmap

from lb.nightly.configuration import DBASE, PARAM, Package, Project, Slot

from .Common import ERROR_EXCEPTIONS, WARNINGS_EXCEPTIONS

s21_first = Slot(
    'lhcb-stripping21-firstpass-patches',
    desc=
    'Maintenance of a patched version of DaVinci for original Stripping 21 full stripping',
    projects=starmap(Project, [('LHCb', 'stripping21-firstpass-patches'),
                               ('Lbcom', 'stripping21-firstpass-patches'),
                               ('Rec', 'stripping21-firstpass-patches'),
                               ('Phys', 'stripping21-firstpass-patches'),
                               ('Analysis', 'stripping21-firstpass-patches'),
                               ('Stripping', 'stripping21-firstpass-patches'),
                               ('DaVinci', 'stripping21-firstpass-patches')]),
    platforms=['x86_64-slc6-gcc48-opt', 'x86_64-slc6-gcc48-dbg'],
    build_tool='cmt',
    env=[
        "PATH=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.17.3/Linux-x86_64/bin:$PATH"
    ],
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS))

for p in s21_first.projects:
    p.checkout_opts['merges'] = 'all'

# setup data packages
s21_first.projects.append(
    DBASE(packages=[Package("PRConfig", "HEAD"),
                    Package("AppConfig", "HEAD")]))

s21 = Slot(
    'lhcb-stripping21-patches',
    desc=
    'Maintenance of a patched version of DaVinci for Stripping 21 incremental restrippings',
    projects=starmap(Project, [('LHCb', 'stripping21-patches'),
                               ('Lbcom', 'stripping21-patches'),
                               ('Rec', 'stripping21-patches'),
                               ('Phys', 'stripping21-patches'),
                               ('Analysis', 'stripping21-patches'),
                               ('Stripping', 'stripping21-patches'),
                               ('DaVinci', 'stripping21-patches')]),
    platforms=['x86_64-slc6-gcc49-opt', 'x86_64-slc6-gcc49-dbg'],
    env=[
        "PATH=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.17.3/Linux-x86_64/bin:$PATH"
    ],
    warning_exceptions=list(WARNINGS_EXCEPTIONS),
    error_exceptions=list(ERROR_EXCEPTIONS))

for p in s21.projects:
    p.checkout_opts['merges'] = 'all'

# setup data packages
s21.projects.append(
    DBASE(packages=[Package("PRConfig", "HEAD"),
                    Package("AppConfig", "HEAD")]))

slots = [s21_first, s21]
